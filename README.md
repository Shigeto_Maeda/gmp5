# README #

This is a GMP(Generative Modeling Project)-5 repository.

### What is this repository for? ###

* For someone who would like to try GMP
* Version - GMP5

### How do I get set up? ###

* Download python code and .blend file,
* Open the 'Blender' and load 'GMP6_sample.blend.
* Find and Push 'Run Script' button at the bottom of Text Window.(left side)

### Where can I change the parameters? ###

* See line #490 - 492, there are three parameters.
* These are basic 'Body' shape parameters.
* Change the numbers and try 'Run Script'.