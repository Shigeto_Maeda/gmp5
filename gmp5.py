#####################################
#
#  NEO_GMP_01 test. 2014_08_11
#
#####################################

from bpy import *
import bpy
import math, mathutils, random

# deleteAllObjects()
def deleteAllObjects():
	bpy.ops.object.select_all(action='SELECT')
	bpy.ops.object.delete(use_global=False)

# addNewPlane
def addNewPlane():
	bpy.ops.mesh.primitive_plane_add(radius=1, view_align=False, enter_editmode=False, location=(0, 0, 0), layers=(True, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False))
	bpy.ops.transform.resize(value=(0.1, 0.1, 0.1), constraint_axis=(False, False, False), constraint_orientation='GLOBAL', mirror=False, proportional='DISABLED', proportional_edit_falloff='SMOOTH', proportional_size=1)

# editMode
def editMode():
	bpy.ops.object.mode_set(mode='EDIT')
	
# objectMode
def objectMode():
	bpy.ops.object.mode_set(mode='OBJECT')

# selectAll
def selectAll():
	bpy.ops.mesh.select_all(action='SELECT')

# deselectAll()
def deselectAll():
	bpy.ops.mesh.select_all(action='DESELECT')
	
# selectMode2Face
def selectMode2Face():
	bpy.ops.mesh.select_mode(type='FACE', action='TOGGLE')

# ext_Face(dt)
def ext_Face(dt):
	bpy.ops.mesh.extrude_faces_move(MESH_OT_extrude_faces_indiv={"mirror":False}, TRANSFORM_OT_shrink_fatten={"value":-dt, "mirror":False, "proportional":'DISABLED', "proportional_edit_falloff":'SMOOTH', "proportional_size":1, "snap":False, "snap_target":'CLOSEST', "snap_point":(0, 0, 0), "snap_align":False, "snap_normal":(0, 0, 0), "release_confirm":False})

# rot_FaceX(degree)
def rot_FaceX(angle):
	bpy.ops.transform.rotate(value=(math.radians(angle)), axis=(-1, -2.22045e-16, -0), constraint_axis=(False, False, False), constraint_orientation='GLOBAL', mirror=False, proportional='DISABLED', proportional_edit_falloff='SMOOTH', proportional_size=1)
	
# scale_Face(size)
def scale_Face(size):
	bpy.ops.transform.resize(value=(size, size, size), constraint_axis=(False, False, False), constraint_orientation='GLOBAL', mirror=False, proportional='DISABLED', proportional_edit_falloff='SMOOTH', proportional_size=1)

# fillFace()
def fillFace():
	bpy.ops.mesh.edge_face_add()

# delHalf
def delHalf():
	editMode()
	deselectAll()
	objectMode()
	ob = bpy.context.object
	me = ob.data
	for p in me.polygons:
		if p.center[0] < 0.0:
			p.select = True
	editMode()
	bpy.ops.mesh.delete(type='FACE')
	objectMode()
	
# addSubSurf(level)
def addSubSurf(level):
	bpy.ops.object.mode_set(mode='OBJECT')
	bpy.ops.object.modifier_add(type='SUBSURF')
	object = bpy.context.active_object
	object.modifiers['Subsurf'].levels = level

# applySubSurf()
def applySubSurf():
	bpy.ops.object.modifier_apply(apply_as='DATA', modifier="Subsurf")

# addMirror()
def addMirror():
	bpy.ops.object.modifier_add(type='MIRROR')

# makeMaterials(num)
def makeMaterials(num):
	for i in range(num + 1):
		matNum = i	
		matName = 'material' + str(matNum)

		mat = bpy.data.materials.new(matName)
		if i == 0:
			mat.diffuse_color = (1.0, 1.0, 1.0)
		else:
			mat.diffuse_color = (random.random(), random.random(), random.random())
			
		mat.texture_slots.add()

		obj = bpy.context.scene.objects.active
		obj.data.materials.append(mat)

# deleteMaterials()
def deleteMaterials():
	for item in bpy.data.materials:
		ob = bpy.context.object
		bpy.data.materials.remove(item)
	
# selectFaces(param)
def selectFaces(param):
	angleFrom = param[0]
	angleTo = param[1]
	placeXFrom = param[2]
	placeXTo = param[3]
	placeYFrom = param[4]
	placeYTo = param[5]
	perc = param[6]
	materialNum = param[7]
	area_limit = param[8]
	targetMaterial = param[9]
	vGroup = 'part1'
	ob = bpy.context.object
	me = ob.data
	newGroup = ob.vertex_groups.new(vGroup)
	objectMode()
	
	up = mathutils.Vector((0.0,0.0,1.0))

	# Lets loop through all the faces in the mesh
	for p in me.polygons:
		# If the angle between up and the face's normal (direction) is smaller than 45... 
		# The face must be pointing up
		# To compare the angle we need 45 degrees in radians, not in degrees!
		# Math with angles is usually all in radians
		if p.normal.angle(up) > math.radians(angleFrom) and p.normal.angle(up) < math.radians(angleTo) and p.center[0] > placeXFrom and p.center[0] < placeXTo and p.center[1] > placeYFrom and p.center[1] < placeYTo and p.area > area_limit and random.random() < perc and (targetMaterial == 99 or targetMaterial == p.material_index) :
			# Set select to true
			p.select = True
			p.material_index = materialNum
		else:
			p.select = False
			
	editMode()
	
	bpy.ops.object.vertex_group_set_active(group=vGroup)
	bpy.ops.object.vertex_group_assign()
	bpy.ops.mesh.select_all(action='DESELECT')

# selMode2FA()
def selMode2FA():
	ob = bpy.context.object
	editMode()
	bpy.ops.mesh.select_mode(type='FACE')
	objectMode()

# selMat(matNum)
def selMat(materialNum):
	ob = bpy.context.object
	editMode()
	bpy.context.object.active_material_index = materialNum
	bpy.ops.object.material_slot_select()
	objectMode()

# extrudeOneStep(oneStep)
def extrudeOneStep(oneStep):
	dZ = oneStep[0] #Kyori
	sX = oneStep[1] #Scale-X
	sY = oneStep[2] #Scale-Y
	rA = oneStep[3] #RotatonAngle
	aX = oneStep[4] #Axis-X
	aY = oneStep[5] #Axis-Y
	aZ = oneStep[6] #Axis-Z
	
	selMode2FA()
	ob = bpy.context.object
	me = ob.data
	# Scale control by face(area) size-->
	context = bpy.context
	if (context.active_object != None):
		object = bpy.context.active_object
	
	# Loop through the faces to find the center
	rotDir = 1	
				
	px = 1
	py = 1
	pz = 1
			
	editMode()
	
	bpy.ops.transform.shrink_fatten(value=dZ * sX, mirror=False, proportional='DISABLED', proportional_edit_falloff='SMOOTH', proportional_size=1.0, snap=False, snap_target='CLOSEST', snap_point=(0.0, 0.0, 0.0), snap_align=False, snap_normal=(0.0, 0.0, 0.0), release_confirm=False)
	bpy.ops.transform.resize(value=(sX, sY, sX), constraint_axis=(True, True, True), constraint_orientation='NORMAL', mirror=False, proportional='DISABLED', proportional_edit_falloff='SMOOTH', proportional_size=0.0762767, snap=False, snap_target='CLOSEST', snap_point=(0, 0, 0), snap_align=False, snap_normal=(0, 0, 0), texture_space=False, release_confirm=False)
	bpy.ops.transform.rotate(value=rA*rotDir, axis=(aX, aY, aZ), constraint_axis=(True, True, True), constraint_orientation='GLOBAL', mirror=False, proportional='DISABLED', proportional_edit_falloff='SMOOTH', proportional_size=1, release_confirm=True)	
	bpy.ops.object.vertex_group_remove_from(use_all_groups=True)
	
	bpy.ops.wm.redraw_timer(type='DRAW_WIN_SWAP', iterations=1)

# multiExtrude()
def multiExtrude(mltData):
	materialNum = mltData[0] 
	mainLevel = mltData[1]
	stepNum = mltData[2] 
	stepDist = mltData[3] 
	stepRot = mltData[4] 
	startDeg = mltData[5] 
	stepDeg = mltData[6] 
	modLevel = mltData[7] 
	modRate = mltData[8]
	axis = mltData[9]
	jointNum = mltData[10]
	newMaterialNum = mltData[11]
	
	ob = bpy.context.object
	me = ob.data

	deselectAll()
	editMode()
	selMat(materialNum)
		
	# Go into object mode so that we are sure we have the current list of faces 
	objectMode()
	# Make a nice list to keep the vertex groups in
	groupList = []

	# Now loop through all the faces
	for i, f in enumerate(ob.data.polygons):
		
		# If this face is selected
		if ob.data.polygons[i].select == True and ob.data.polygons[i].area > 0.07:

			# Create a new vertex group!
			newGroup = ob.vertex_groups.new('mygroup')
			groupList.append(newGroup)

			# Get all the vertices in the current face and add them to the new group
			for v in f.vertices:
				newGroup.add([v], 1.0, 'REPLACE')
			
	# Now we loop through the groups and do what we want.
	for g in groupList:

		# Make sure nothing is selected
		editMode()
		deselectAll()
		# Make the group in our list the active one
		ob.vertex_groups.active_index = g.index

		# Select all the verts in the active group
		bpy.ops.object.vertex_group_select()

		bpy.ops.object.vertex_group_remove(all=False)	

		# AND NOW WE CAN DO OUR STUFF... little test example follows
		t = startDeg
		oldValue = 1.0
		oldValue2 = 1.0
		# Shape
		j = 0
		for i in range(stepNum):
     
			bpy.ops.mesh.extrude_region_move(MESH_OT_extrude_region={"mirror":False}, TRANSFORM_OT_translate={"value":(0, 0, 0), "constraint_axis":(False, False, True), "constraint_orientation":'NORMAL', "mirror":False, "proportional":'DISABLED', "proportional_edit_falloff":'SMOOTH', "proportional_size":1, "snap":False, "snap_target":'CLOSEST', "snap_point":(0, 0, 0), "snap_align":False, "snap_normal":(0, 0, 0), "texture_space":False, "remove_on_cancel":False, "release_confirm":False})
			
			# An added trick... remove the extruded face from all vertex groups after the first extrusion (i == 0)
			# This way it can't get extruded again
			# Because the edge of the first face can be part of multiple groups
			if not i:
				bpy.ops.object.vertex_group_remove_from(use_all_groups=True)

			editMode()
			
			newValue = (math.sin(math.radians(t)+math.sin(math.radians(t/1.13)))+2.0)*mainLevel  #*2
			newValue2 = (math.sin(math.radians(t*0.5)+math.sin(math.radians(t/0.97)))+2.0)*mainLevel
			newRatio = newValue/oldValue
			newRatio2 = newValue2/oldValue2
				
			# Get the mesh
			me = ob.data
			# Loop through the faces to find the center
			area = 1
			for f in me.polygons:
				if f.select == True:
					area = math.sqrt(f.area) * 3
			
			dA = stepDist * area
			sA = newRatio
			sA2 = newRatio2
			rA = modLevel*math.sin(math.radians(t*modRate))*(-stepRot/10)
#			rA = math.sin(t*0.3) * 0.5
			axis_x = axis[0]
			axis_y = axis[1]
			axis_z = axis[2]
			axis_z = 0.5
			#rZ = math.sin(math.radians(i*45 + bodyNum+45))*0.5 #<---------------------------------------
			rZ = 0
				
			morphData = [dA, sA, sA, rA, axis_x, axis_y, axis_z,rZ]
			extrudeOneStep(morphData)
			
			if jointNum != 0:
				j = j+1
				if j == jointNum:
					bpy.ops.mesh.extrude_region()
					oneStep = [ -0.01, 0.6, 0.6, 0, axis_x, axis_y, axis_z ]
					extrudeOneStep(oneStep) # <-------------0
					bpy.ops.mesh.extrude_region()
					oneStep = [ -0.01, 1.66, 1.66, 0, axis_x, axis_y, axis_z ]
					extrudeOneStep(oneStep) # <-------------0
					j = 0
			t = t + stepDeg
			oldValue = newValue
			oldValue2 = newValue2
			
		if materialNum != newMaterialNum:
			objectMode()
			for p in me.polygons:
				if p.select:
					p.material_index = newMaterialNum
			
	editMode()

# makeEyes()
def makeEyes():
	ob = bpy.context.object
	me = ob.data

	deselectAll()
	editMode()
	selMat(materialNum)
		
	# Go into object mode so that we are sure we have the current list of faces 
	objectMode()
	# Make a nice list to keep the vertex groups in
	groupList = []

	# Now loop through all the faces
	for i, f in enumerate(me.polygons):
		
		# If this face is selected
		if me.polygons[i].select == True:

			# Create a new vertex group!
			newGroup = ob.vertex_groups.new('mygroup')
			groupList.append(newGroup)

			# Get all the vertices in the current face and add them to the new group
			for v in f.vertices:
				newGroup.add([v], 1.0, 'REPLACE')
			
	# Now we loop through the groups and do what we want.
	for g in groupList:

		# Make sure nothing is selected
		editMode()
		deselectAll()
		# Make the group in our list the active one
		ob.vertex_groups.active_index = g.index

		# Select all the verts in the active group
		bpy.ops.object.vertex_group_select()

		bpy.ops.object.vertex_group_remove(all=False)	

		# AND NOW WE CAN DO OUR STUFF... little test example follows
		# Shape the eyes
		dFactor = [ 0.0, -0.2, 0.0, 0.0, -0.5, -1.0 ]
		mFactor = [ 0.8, 1.0, 0.9, 1.0, 0.9, 0.5 ]
		for i in range(6):
			
			bpy.ops.mesh.extrude_region_move(MESH_OT_extrude_region={"mirror":False}, TRANSFORM_OT_translate={"value":(0, 0, 0), "constraint_axis":(False, False, True), "constraint_orientation":'NORMAL', "mirror":False, "proportional":'DISABLED', "proportional_edit_falloff":'SMOOTH', "proportional_size":1, "snap":False, "snap_target":'CLOSEST', "snap_point":(0, 0, 0), "snap_align":False, "snap_normal":(0, 0, 0), "texture_space":False, "remove_on_cancel":False, "release_confirm":False})
			
			if not i:
				bpy.ops.object.vertex_group_remove_from(use_all_groups=True)

			dA = dFactor[ i ]
			sA = mFactor[ i ]
			sA2 = mFactor[ i ]
			rA = 0.0
			axis_x = 0
			axis_y = 0
			axis_z = 1
			rZ = 0
				
			morphData = [dA, sA, sA, rA, axis_x, axis_y, axis_z, rZ]
			extrudeOneStep(morphData)
			
	editMode()
	
# makeFins()
def makeFins():
	ob = bpy.context.object
	me = ob.data

	deselectAll()
	editMode()
	selMat(materialNum)
		
	# Go into object mode so that we are sure we have the current list of faces 
	objectMode()
	# Make a nice list to keep the vertex groups in
	groupList = []

	# Now loop through all the faces
	for i, f in enumerate(me.polygons):
		
		# If this face is selected
		if me.polygons[i].select == True:

			# Create a new vertex group!
			newGroup = ob.vertex_groups.new('mygroup')
			groupList.append(newGroup)

			# Get all the vertices in the current face and add them to the new group
			for v in f.vertices:
				newGroup.add([v], 1.0, 'REPLACE')
			
	# Now we loop through the groups and do what we want.
	for g in groupList:

		# Make sure nothing is selected
		editMode()
		deselectAll()
		# Make the group in our list the active one
		ob.vertex_groups.active_index = g.index

		# Select all the verts in the active group
		bpy.ops.object.vertex_group_select()

		bpy.ops.object.vertex_group_remove(all=False)	

		# AND NOW WE CAN DO OUR STUFF... little test example follows
		# Shape the fins
		dFactor = [ -1.5, -1.5, -4.0, -4.0, -2.0 ]
		mxFactor = [ 0.5, 1.0, 1.0, 1.0, 0.5 ]
		myFactor = [ 0.5, 3.0, 0.9, 0.5, 0.5 ]
		rFactor = [ -50.0, 0.0, 50.0, 0.0, 0.0 ]
		for i in range(5):
			
			bpy.ops.mesh.extrude_region_move(MESH_OT_extrude_region={"mirror":False}, TRANSFORM_OT_translate={"value":(0, 0, 0), "constraint_axis":(False, False, True), "constraint_orientation":'NORMAL', "mirror":False, "proportional":'DISABLED', "proportional_edit_falloff":'SMOOTH', "proportional_size":1, "snap":False, "snap_target":'CLOSEST', "snap_point":(0, 0, 0), "snap_align":False, "snap_normal":(0, 0, 0), "texture_space":False, "remove_on_cancel":False, "release_confirm":False})
			
			if not i:
				bpy.ops.object.vertex_group_remove_from(use_all_groups=True)

			dA = dFactor[ i ]
			sA = mxFactor[ i ]
			sA2 = myFactor[ i ]
			rA = rFactor[ i ]
			axis_x = 0
			axis_y = 1
			axis_z = 0
			rZ = 0
				
			morphData = [dA, sA, sA2, rA, axis_x, axis_y, axis_z, rZ]
			extrudeOneStep(morphData)
			
	editMode()
				
################################################################################################################################## main program
deleteAllObjects()
deleteMaterials()

######### Parameters ########
materials = 15
baseDist = 0.8
totalStep = 13
freqA = 0.5 #0.6 -> 1.2
phaseA = -1.0
freqB = -0.1
phaseB = -0.1
amplitude = 3.0
###########################

##### MAKE A BODY! ########
addNewPlane() #-------------------------------------------------- First Seed of Object
#bpy.context.object.rotation_euler[0] = -1.5708
editMode()
rot_FaceX(0)
objectMode()

makeMaterials(materials) #--------------------------------------- Make materials for Parts as Legs, etc.

ob = bpy.context.object
me = ob.data

editMode()
selectAll()
selectMode2Face()

oldRatio = 1.0

######################## <<<<<<< Body shape parameters !!!!!
bPhase = 30
bMod = -20
bAmp = 23
########################

for i in range(totalStep):
	if i < totalStep / 3:
		mNum = 1
	elif i < totalStep / 3 * 2:
		mNum = 2
	else:
		mNum = 3
	objectMode()
	for p in me.polygons:
		if p.select:
			dtRatio = math.sqrt(p.area)
			p.material_index = mNum
	editMode()
	ext_Face(baseDist * dtRatio)
	rotDeg = math.sin(math.radians((i-bPhase) * bMod)) * bAmp
	rot_FaceX(rotDeg)
	
	newRatio = amplitude * math.sin(freqA * i + phaseA) * math.sin(freqB * i + phaseB) + (amplitude + 1)
	magni = newRatio/oldRatio
	scale_Face(magni)
	oldRatio = newRatio
ext_Face(baseDist * dtRatio)
scale_Face(0.2)

selectAll()
fillFace()
deselectAll()

######### Parameters ########
angleFrom = 80.0
angleTo = 180.0
placeXFrom = -0.1
placeXTo = 10.0
placeYFrom = -80.0
placeYTo = 80.0
perc = 0.6
materialNum = 12
area_limit = 10.0
targetMaterial = 3
###########################

param = [ angleFrom, angleTo, placeXFrom, placeXTo, placeYFrom, placeYTo, perc, materialNum, area_limit, targetMaterial ]
selectFaces(param)

###########################
materialNum = 12 
mainLevel = 0.2
stepNum = 1
stepDist = -0.2 
stepRot = 60 
startDeg = 60
stepDeg = 40
modLevel = 0.0
modRate = 0.0
axis = [0, 0, 0]
jointNum = 0
newMaterialNum = 12
###########################

mltData = [ materialNum, mainLevel, stepNum, stepDist, stepRot, startDeg, stepDeg, modLevel, modRate, axis, jointNum, newMaterialNum ]
multiExtrude(mltData)

addSubSurf(1)
applySubSurf()
delHalf()
addMirror()

#aaa # stopper

############################

# SELECT SOME FACES (part 1 - tail spine) #
# select with the angle, place(x, y) and percent of each faces

######### Parameters ########
angleFrom = 30.0
angleTo = 120.0
placeXFrom = 0.0
placeXTo = 5.0
placeYFrom = -5.0
placeYTo = 5.0
perc = 0.5
materialNum = 5
area_limit = 0.0
targetMaterial = 1
###########################

param = [ angleFrom, angleTo, placeXFrom, placeXTo, placeYFrom, placeYTo, perc, materialNum, area_limit, targetMaterial ]
selectFaces(param)

# SELECT SOME FACES (part 2 - legs) #
# select with the angle, place(x, y) and percent of each faces

######### Parameters ########
angleFrom = 60.0
angleTo = 120.0
placeXFrom = 3.0
placeXTo = 8.0
placeYFrom =-80.0
placeYTo = 80.0
perc = 0.8
materialNum = 6
area_limit = 5.0
targetMaterial = 3
###########################

param = [ angleFrom, angleTo, placeXFrom, placeXTo, placeYFrom, placeYTo, perc, materialNum, area_limit, targetMaterial ]
selectFaces(param)

# SELECT FACES BY MATERIAL #

###########################
materialNum = 5 
mainLevel = 0.5
stepNum = 4
stepDist = -0.2 
stepRot = 80 
startDeg = 0
stepDeg = 20
modLevel = 0.2
modRate = 0.2
axis = [0, 1, 0]
jointNum = 0
newMaterialNum = 7
###########################

mltData = [ materialNum, mainLevel, stepNum, stepDist, stepRot, startDeg, stepDeg, modLevel, modRate, axis, jointNum, newMaterialNum ]
multiExtrude(mltData)

###########################
materialNum = 6 
mainLevel = 0.3
stepNum = 3
stepDist = -1.5 
stepRot = -60 
startDeg = 0
stepDeg = 90
modLevel = 0.2
modRate = 0.5
axis = [0, 1, 0]
jointNum = 1
newMaterialNum = 8
###########################

mltData = [ materialNum, mainLevel, stepNum, stepDist, stepRot, startDeg, stepDeg, modLevel, modRate, axis, jointNum, newMaterialNum ]
multiExtrude(mltData)

###########################
materialNum = 7 
mainLevel = 0.2
stepNum = 7
stepDist = -0.2 
stepRot = 30 
startDeg = 0
stepDeg = 40
modLevel = 0.2
modRate = 0.2
axis = [-1, -1, 0]
jointNum = 0
newMaterialNum = 7
###########################

mltData = [ materialNum, mainLevel, stepNum, stepDist, stepRot, startDeg, stepDeg, modLevel, modRate, axis, jointNum, newMaterialNum ]
multiExtrude(mltData)

###########################
materialNum = 8 
mainLevel = 0.45
stepNum = 7
stepDist = -0.2 
stepRot = -30 
startDeg = 0
stepDeg = 40
modLevel = 0.2
modRate = 0.2
axis = [0, 1, 0]
jointNum = 0
newMaterialNum = 8
###########################

mltData = [ materialNum, mainLevel, stepNum, stepDist, stepRot, startDeg, stepDeg, modLevel, modRate, axis, jointNum, newMaterialNum ]
multiExtrude(mltData)
deselectAll()
# SELECT SOME FACES (part 4 - fins) #
# select with the angle, place(x, y) and percent of each faces

######### Parameters ########
angleFrom = 150.0
angleTo = 180.0
placeXFrom = 0.0
placeXTo = 5.0
placeYFrom = -80.0
placeYTo = 80.0
perc = 1.0
materialNum = 9
area_limit = 5.0
targetMaterial = 2
###########################

param = [ angleFrom, angleTo, placeXFrom, placeXTo, placeYFrom, placeYTo, perc, materialNum, area_limit, targetMaterial ]
selectFaces(param)

###########################
materialNum = 9 
mainLevel = 0.2
stepNum = 7
stepDist = -0.2 
stepRot = 60 
startDeg = 60
stepDeg = 40
modLevel = 0.1
modRate = 0.1
axis = [0, 1, 0]
jointNum = 0
newMaterialNum = 9
###########################

mltData = [ materialNum, mainLevel, stepNum, stepDist, stepRot, startDeg, stepDeg, modLevel, modRate, axis, jointNum, newMaterialNum ]
multiExtrude(mltData)
deselectAll()

# SELECT SOME FACES (part 5 - spines) #
# select with the angle, place(x, y) and percent of each faces

######### Parameters ########
angleFrom = 0.0
angleTo = 90.0
placeXFrom = 0.0
placeXTo = 4.0
placeYFrom = -80.0
placeYTo = 80.0
perc = 1.0
materialNum = 10
area_limit = 15.0
targetMaterial = 3
###########################

param = [ angleFrom, angleTo, placeXFrom, placeXTo, placeYFrom, placeYTo, perc, materialNum, area_limit, targetMaterial ]
selectFaces(param)

###########################
materialNum = 10 
mainLevel = 0.5
stepNum = 4
stepDist = -0.2 
stepRot = 60 
startDeg = 0
stepDeg = 80
modLevel = 0.2
modRate = 0.1
axis = [1, 0, 0]
jointNum = 0
newMaterialNum = 11
###########################

mltData = [ materialNum, mainLevel, stepNum, stepDist, stepRot, startDeg, stepDeg, modLevel, modRate, axis, jointNum, newMaterialNum ]
multiExtrude(mltData)
deselectAll()
# SELECT SOME FACES (part 3 - eyes) #
# select with the angle, place(x, y) and percent of each faces

######### Parameters ########
angleFrom = 0.0
angleTo = 90.0
placeXFrom = 0.0
placeXTo = 3.0
placeYFrom = -80.0
placeYTo = 80.0
perc = 1.0
materialNum = 4
area_limit = 8.0
targetMaterial = 1
###########################

param = [ angleFrom, angleTo, placeXFrom, placeXTo, placeYFrom, placeYTo, perc, materialNum, area_limit, targetMaterial ]
selectFaces(param)

makeEyes()
deselectAll()

addSubSurf(1)
applySubSurf()
# SELECT SOME FACES (part 6 - spines2) #
# select with the angle, place(x, y) and percent of each faces

######### Parameters ########
angleFrom = 0.0
angleTo = 60.0
placeXFrom = 0.0
placeXTo = 3.0
placeYFrom = -80.0
placeYTo = 80.0
perc = 1.0
materialNum = 10
area_limit = 7.0
targetMaterial = 11
###########################

param = [ angleFrom, angleTo, placeXFrom, placeXTo, placeYFrom, placeYTo, perc, materialNum, area_limit, targetMaterial ]
selectFaces(param)

###########################
materialNum = 11 
mainLevel = 0.3
stepNum = 7
stepDist = -0.3 
stepRot = 60 
startDeg = 0
stepDeg = 40
modLevel = 0.2
modRate = 0.1
axis = [0, 1, 0]
jointNum = 0
newMaterialNum = 11
###########################

mltData = [ materialNum, mainLevel, stepNum, stepDist, stepRot, startDeg, stepDeg, modLevel, modRate, axis, jointNum, newMaterialNum ]
multiExtrude(mltData)
deselectAll()

addSubSurf(1)

for i in range(materials + 1):
	bpy.ops.object.material_slot_remove()